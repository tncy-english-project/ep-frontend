import axios from "axios";

const api = axios.create({
    withCredentials: true,
    baseURL: import.meta.env.VITE_URL_BACKEND,
});

export default api;