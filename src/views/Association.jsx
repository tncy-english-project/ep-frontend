import { useState, useEffect } from 'react'
import api from '../../api/axios-auth.js'


function Association(){

  const [mot, setMot] = useState('FEUR')
  const [reponse, setReponse] = useState('')

  const ws = new WebSocket('ws://localhost:8000/api/v1/gateway')

  ws.onmessage = (event) => {
    const data = JSON.parse(event.data);
    console.log('Received data:', data);
  };  

  return (
    <div>
      <h1>{mot}</h1>
      <input type="text" onChange={(e) => setReponse(e.target.value)} placeholder="Type your answer" className="input w-full max-w-xs" />
    </div>
    )
}

export default Association