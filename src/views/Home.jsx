import { useState } from 'react'
import {Routes, Route, Link} from 'react-router-dom'

function Home() {

  return (
    <div>

      <p>Page Home</p>
      <Link to="/">Home</Link>
      <Link to="/memory">Memory</Link>
      <Link to="/association">Association</Link>
      <Link to="/login">Login</Link>
      <Link to="/signup">Signup</Link>
    </div>
  )
}

export default Home