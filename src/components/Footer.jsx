import { useState } from 'react'
import {Routes, Route} from 'react-router-dom'


function Footer(){

  return ( 

<footer class="fixed bottom-0 left-0 z-20 w-full p-4 bg-white border-t border-gray-100 shadow md:flex md:items-center md:justify-between md:p-6 dark:bg-gray-800 dark:border-gray-600">
<p class="mx-auto">Fait avec ❤️ par Malo et Tristan</p> 
</footer>

    )
}

export default Footer