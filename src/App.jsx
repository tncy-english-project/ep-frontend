import { useState } from 'react'
import {BrowserRouter, Routes, Route, Link} from 'react-router-dom'
import './App.css'
import Home from './views/Home'
import Memory from './views/Memory'
import Association from './views/Association'
import Login from './views/Login'
import Signup from './views/Signup'
import Footer from './components/Footer'

function App() {
  const [token, setToken] = useState();

  return (
    <div className='App'>
      
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/memory" element={<Memory />} />
        <Route path="/association" element={<Association />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<Signup />} />
      </Routes>
      <Footer />
    </div>
  )
}

export default App
